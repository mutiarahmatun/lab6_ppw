# Lab-6
## Authors
* **Mutia Rahmatun Husna** - 1706039622

## Links
* Herokuapp: https://lifeishere19.herokuapp.com/

## Pipeline status
[![pipeline status](https://gitlab.com/mutiarahmatun/lab6_ppw/badges/master/pipeline.svg)](https://gitlab.com/mutiarahmatun/lab6_ppw/commits/master)
## Coverage report
[![coverage report](https://gitlab.com/mutiarahmatun/lab6_ppw/badges/master/coverage.svg)](https://gitlab.com/mutiarahmatun/lab6_ppw/commits/master)