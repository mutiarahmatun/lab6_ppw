from django.contrib import admin
from .models import Status

# Register your models here.

class StatusAdmin(admin.ModelAdmin):
    list_display = ('status', 'waktu')
    list_display_links = ('status',)
    search_fields = ('status',)

admin.site.register(Status, StatusAdmin)