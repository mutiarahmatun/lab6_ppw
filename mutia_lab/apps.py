from django.apps import AppConfig

class MutiaLabConfig(AppConfig):
    name = 'mutia_lab'
