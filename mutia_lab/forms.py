from django import forms
from .models import Status

class Status_Form(forms.ModelForm) :

	class Meta:
		model = Status
		fields = ['status']
		widgets = {
            'status': forms.TextInput(attrs={'class': 'form-control',
            	'placeholder': 'Isi status', 'maxlength':300}),
            'waktu': forms.HiddenInput() #hidden input
        }