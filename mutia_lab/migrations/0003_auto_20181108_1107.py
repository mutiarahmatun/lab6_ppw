# Generated by Django 2.1.1 on 2018-11-08 04:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mutia_lab', '0002_auto_20181107_1823'),
    ]

    operations = [
        migrations.AlterField(
            model_name='status',
            name='status',
            field=models.TextField(max_length=300, null=True),
        ),
    ]
