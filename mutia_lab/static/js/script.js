$(document).ready(
    function () {
    $("#accordion").accordion({
        header: "h3",
        active: false,
        collapsible: true,
        heightStyle: "content",
        navigation: true
    });

    $(".ui-accordion-content").css("background-color", "#1abc9c");
    $(".ui-accordion-header").css("background-color", "#1abc9c");

    document.getElementById('switch').onclick = function () {

        if (document.getElementById('theme').href == "/static/css/web-dark.css") 
        {
            document.getElementById('theme').href = "/static/css/web-style.css";
        } 
        
        else {
            document.getElementById('theme').href = "/static/css/web-dark.css";
        }
    };
});

$(window).load(function() {
    // Animate loader off screen
    $(".se-pre-con").delay(100).fadeOut("slow");
});
$(document).ready(function () {
    $("input").change(function () {
        var search = $("input").val();
        $("#cari").click(function() {
            $.ajax({
                url: "/dtJSON/" + search,
                success: function(data) {
                  var ev_data = "";
                  var dataOfBook = data.items;
                  for (var i = 0; i < dataOfBook.length; i++) {
                    ev_data += '<tbody><tr id ="tableBook" class="ok"> ';
                    ev_data += "<td>" + data.items[i].volumeInfo.title + "</td>";
                    ev_data += "<td><img src='" + data.items[i].volumeInfo.imageLinks.thumbnail  + "'></td>";
                    ev_data += "<td>" + data.items[i].volumeInfo.authors + "</td>";
                    ev_data += "<td>" + data.items[i].volumeInfo.publisher + "</td>";
                    ev_data += "<td>" + data.items[i].volumeInfo.publishedDate + "</td>";
                    ev_data +=
                      '<td align ="center"><a href="#" class="btn btn-default buttonLove" title="count to favourite"><i class="glyphicon glyphicon-star" ></></a></td>';
                    ev_data += "</tr></tbody>";
                  }
                  $("#table_json").append(ev_data);
                }
            });
        })
    });
});

// button when clicked
$(document).on('click','.buttonLove',function(){
    $('#counter').val(parseInt($('#counter').val()) + 1 );
    $(this).toggleClass("buttonLoveClicked");
});