from django.test import TestCase, Client
from django.urls import resolve
from django.utils import timezone
from django.http import HttpRequest

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

from .models import Status
from .forms import Status_Form
from .views import status, home, myBook
import time
#import unittest

# Create your tests here.
class Lab6UnitTest(TestCase):

	def test_lab_6_url_is_exist(self):
		response = Client().get('/home/')
		self.assertEqual(response.status_code, 200)
		
	def test_lab6_using_home_func(self):
		found = resolve('/home/')
		self.assertEqual(found.func, home)

	def test_lab6_using_status_func(self):
		found = resolve('/status/')
		self.assertEqual(found.func, status)

	def test_model_can_create_new_status(self):
		# Creating a new status
		new_status = Status.objects.create(status = 'mengerjakan story 9 ppw')
		# Retrieving all status
		counting_all_status_message = Status.objects.all().count()
		self.assertEqual(counting_all_status_message, 1)

	def test_lab6_using_profil_template(self):
		response = Client().get('/home/')
		self.assertTemplateUsed(response, 'home.html')

	def test_status_validation_for_blank_items(self):
		form = Status_Form(data={'status': ''})
		self.assertFalse(form.is_valid())
		self.assertEqual(
			form.errors['status'],
			["This field is required."]
		)

class Lab6FunctionalTest(TestCase):

	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('disable-gpu')
		self.selenium = webdriver.Chrome(
			'./chromedriver', chrome_options=chrome_options)
		super(Lab6FunctionalTest, self).setUp()

	def tearDown(self):
		self.selenium.quit()
		super(Lab6FunctionalTest, self).tearDown()
		
"""	def test_input_todo(self):
		selenium = self.selenium
		
		# Opening the link we want to test
		selenium.get('/status/')
		time.sleep(5)
		
		# find the form element
		status = selenium.find_element_by_id('id_status')
		add = selenium.find_element_by_id('add')
		
		# Fill the form with data
		hari.send_keys('Kamis')
		time.sleep(5)
		status.send_keys('Coba-coba')
		time.sleep(5)
		
		# submitting the form
		add.send_keys(Keys.RETURN)
		time.sleep(5)
		
		self.assertIn('Coba-coba', self.selenium.page_source)
		
	def test_for_positition_hari_position(self):
		selenium = self.selenium
		
		# Opening the link we want to test
		selenium.get('https://lifeishere19.herokuapp.com/status/')
		
		posisi_hari = selenium.find_element_by_id('id_hari')
		location = posisi_hari.location
		lokasi_hari = location['x']
		self.assertEqual(130, lokasi_hari)
		
	def test_for_positition_status_position(self):
		selenium = self.selenium
		
		# Opening the link we want to test
		selenium.get('https://lifeishere19.herokuapp.com/status/')
		
		posisi_status = selenium.find_element_by_id('id_status')
		location = posisi_status.location
		lokasi_status = location['x']
		self.assertEqual(456, lokasi_status)
		
	def test_for_size_hari(self):
		selenium = self.selenium
		
		# Opening the link we want to test
		selenium.get('https://lifeishere19.herokuapp.com/status/')
		
		size_hari = selenium.find_element_by_id('id_hari')
		sizee = size_hari.size
		ukuran = sizee['height']
		self.assertEqual(42, ukuran)
	
	def test_for_size_status(self):
		selenium = self.selenium
		
		# Opening the link we want to test
		selenium.get('https://lifeishere19.herokuapp.com/status/')
		
		size_status = selenium.find_element_by_id('id_status')
		sizee = size_status.size
		ukuran = sizee['height']
		self.assertEqual(42, ukuran)"""