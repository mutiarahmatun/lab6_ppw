from django.urls import path
from mutia_lab import views

urlpatterns = [
	path('status/', views.status, name='status'),
	path('home/', views.home, name='home'),
	path('', views.home, name='home'),
	path('book/', views.myBook, name='myBook'),
	path('dtJSON/<str:value>', views.dtJSON),
]
