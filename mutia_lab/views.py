from django.shortcuts import render
from django.urls import reverse
from django.http import HttpResponseRedirect, HttpResponse
from .models import Status
from .forms import Status_Form
from django.core.paginator import Paginator
import urllib.request, json, requests

# Create your views here.
landing_page = 'Hello, Apa Kabar?'

def home(request):
	return render(request, 'home.html')
	
def status(request):
	form = Status_Form(request.POST or None)

	if (form.is_valid()):
		form.save()
		return HttpResponseRedirect(reverse('status'))
	else:
		form = Status_Form()

	status_list = Status.objects.all().order_by('-waktu')  # sorted by creation datetime
	paginator = Paginator(status_list, 10)
	page = request.GET.get('page')
	status_paginate = paginator.get_page(page)
	response = {'form': form, 'status_list' : status_paginate, 'landing_page' : landing_page }
	return render(request, "forms.html", response )

def myBook(request): 
    return render(request, 'books.html', {})

def dtJSON(request, value):
    data = requests.get('https://www.googleapis.com/books/v1/volumes?q=' + value)
    return HttpResponse(data, content_type='application/json')