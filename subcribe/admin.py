from django.contrib import admin
from .models import Subcribe

# Register your models here.

class SubcribeAdmin(admin.ModelAdmin):
    list_display = ['nama', 'email', 'password']

admin.site.register(Subcribe, SubcribeAdmin)
