from django import forms
from .models import Subcribe

class Subcribe_Form(forms.ModelForm) :

	class Meta:
		model = Subcribe
		fields = ['nama', 'email', 'password']
		widgets = {
			'nama': forms.TextInput(
				attrs={'class': 'form-control', 'id' : 'nama_form',
					   'placeholder': 'Isi nama', 'maxlength':30}),

			'email' : forms.TextInput(
				attrs={'type' : 'email', 'class':'form-control', 'id':'email_form',
					   'placeholder': 'Isi email', 'maxlength': 50}),

			'password':forms.TextInput(
				attrs={'type': 'password', 'class': 'form-control', 'id': 'password_form',
					   'placeholder': 'Isi password', 'maxlength': 30}),

		}