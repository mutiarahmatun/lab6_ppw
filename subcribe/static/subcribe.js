// ajax function for create user POST method
$(document).ready(
    function subcribe() {

        $.ajax({
            url: "/welcome/subcribe/",
            type: "POST",
            data: {
                nama: $('#nama_form').val(),
                email: $('#email_form').val(),
                password: $('#password_form').val()
            },

            success: function (json) {
                // console.log(json);
                $('#subcribe_form').val(''); // empty form
                $('#msg_response').html("<div class='alert alert-success'><strong>Success!</strong> Account has been created.</div>");
            },

            error: function (xhr, errmsg, err) {
                $('#msg_response').html("<div class='alert alert-danger'><strong>Error!</strong>Something has wrong.</div>");
            },
        })
    });

    function validate_user() {
        $.ajax({
            url: "/welcome/validate/",
            type: "POST",
            data: {
                nama: $('#nama_form').val(),
                email: $('#email_form').val(),
                password: $('#password_form').val()

            },

            success: function (response) {

                console.log(response.message)
                if (response.message == "All fields are valid"){

                    document.getElementById('subcribe_button').disabled = false;
                    $('#msg_validate').html("<p style='color:green'>"+ response.message + "</p>")

                }

                else {

                    document.getElementById('subcribe_button').disabled = true;
                    $('#msg_validate').html("<p style='color:red'>"+ response.message + "</p>")
                }
            },

            // debugging
            error: function (errmsg) {

                console.log(errmsg + "ERROR DETECTED");
            }

        });
    }

        var x_timer;
        $("#email_form").keyup(function (e) {
            clearTimeout(x_timer);
            var email = $(this).val();
            x_timer = setTimeout(function () {
                validate_user();
            }, 10);
        });


        var x_timer;
        $("#nama_form").keyup(function (e) {
            clearTimeout(x_timer);
            var password = $(this).val();
            x_timer = setTimeout(function () {
                validate_user();

            }, 10);

        });

        var x_timer;
        $("#password_form").keyup(function (e) {
            clearTimeout(x_timer);
            var nama = $(this).val();
            x_timer = setTimeout(function () {
                validate_user();
            }, 10);
        });

