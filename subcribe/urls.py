from django.urls import path
from subcribe import views

urlpatterns = [
    path('subscribe/', views.subcribe , name="subcribe_url"),
    path('validate/', views.user_validate , name="validate_email"),
    #path('unsubscribe/' , views.unsubscribe , name="unsubscribe_email")
]
