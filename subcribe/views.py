from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse
from .forms import Subcribe_Form
from .models import Subcribe
import json, re

# Create your views here.

# function POST request subscriber
@csrf_exempt
def subcribe(request):

    # pagination page
    get_user_acccount = Subcribe.objects.all()

    # post method save user
    if request.method == "POST":
        form_value = Subcribe_Form(request.POST or None)
        nama = request.POST['nama']
        email = request.POST['email']
        password = request.POST['password']
        user = Subcribe(nama=nama, email=email, password=password)
        user.save()

    else:
        form_value = Subcribe_Form()

    return render(request, 'page_subcribe.html', {'the_form': form_value})


# function for validate email user , name , password
@csrf_exempt
def user_validate(request):

    # boolean flag for chack each form
    nama_validate_check = False
    password_validate_check = False
    email_validate_check = False

    if request.method == "POST":
        nama = request.POST['nama']
        email = request.POST['email']
        password = request.POST['password']
        print(nama, email, password)
        # using regex check validate email address
        if len(email) >= 8:
            email_validate_check = bool(re.match("^.+@(\[?)[a-zA-Z0-9-.]+.([a-zA-Z]{2,3}|[0-9]{1,3})(]?)$", email))

        if 8 <= len(password) <= 14:
            password_validate_check = True

        if 5 <= len(nama) <= 30:
            nama_validate_check = True

        if not nama_validate_check:
            return HttpResponse(json.dumps({'message': 'Name must be 5 to 30 characters or fewer.'}),
                                content_type="application/json")

        if not email_validate_check:
            return HttpResponse(json.dumps({'message': 'email must contain "@" character'}),
                                content_type="application/json")

        if not password_validate_check:
            return HttpResponse(json.dumps({'message': 'Your password must contain at least 8 characters'}),
                                content_type="application/json")

        user_filter_email = Subcribe.objects.filter(email=email)

        if len(user_filter_email) > 0:
            return HttpResponse(json.dumps({'message': 'Email already registered'}), content_type="application/json")

        return HttpResponse(json.dumps({'message': 'All fields are valid'}), content_type="application/json")

    else:
        return HttpResponse(json.dumps({'message': "There's something wrong. Try again."}),
                            content_type="application/json")